package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	private List<Ingredient> ingredient;
	
	public PizzaCreateDto() {}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setIngredients(List<Ingredient> ingredient) {
		this.ingredient = ingredient;
	}
	
	public List<Ingredient> getIngredient() {
		return ingredient;
	}
	
}
