package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public class PizzaDto {
	
	private long id;
	private String nom;
	private List<Ingredient> ingredient;
	
	public PizzaDto() {};
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return nom;
	}
	
	public void setName(String nom) {
		this.nom = nom;
	}
	
	public List<Ingredient> getIngredients() {
		return ingredient;
	}
	
	public void setIngredients(List<Ingredient> ingredient) {
		this.ingredient = ingredient;
	}
	
}
