package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredient;

	public Pizza() {
	}

	public Pizza(long id, String name, List<Ingredient> ingredient) {
		this.id = id;
		this.name = name;
		this.ingredient = ingredient;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIngredient(List<Ingredient> ingredient) {
		this.ingredient = ingredient;
	}

	public List<Ingredient> getIngredients() {
		return this.ingredient;
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setIngredients(p.getIngredients());

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		pizza.setIngredient(dto.getIngredients());

		return pizza;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String retour = "Pizza [id=" + id + ", name=" + name;
		for (Ingredient unIngredient : ingredient) {
			retour += ", ingredient=" + unIngredient;
		}
		retour += "]";
		return retour;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		dto.setIngredients(pizza.getIngredients());
		
		return dto;
	}
	
	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		pizza.setIngredient(dto.getIngredient());
		
		return pizza;
	}

}
